## Accela Tech Test
  - Repo for Accela technical test
  - This is a cross platform application built using Ionic Angular

# Setup

Required tools:
  - NodeJS

Install:
  - Ionic CLI is required
  `npm install -g @ionic/cli native-run cordova-res`
  - Clone repo
  - Install required modules
  `npm install`

  Run In Browser:
  - `ionic cordova build browser`
  - `ionic cordova run browser`
  
  Run on ios (Mac Required):
  - `ionic cordova prepare ios`
  - `ionic cordova run ios`
  
  Run on Android:
  - `ionic cordova prepare android`
  - `ionic cordova run android`

