import { Component } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage-angular';
import { Router } from  "@angular/router";

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  //variables
  posts : [];
  userId;

  constructor(private router: Router,private http: HTTP,private storage: Storage) {}

  ngOnInit() {

    //get user data
    this.storage.get('user').then((val) => {
      this.userId = val.id;
    });

    //on page load get user posts 
    this.http.get('https://jsonplaceholder.typicode.com/posts', {}, {})
      .then(data => {
        this.posts = JSON.parse(data.data) // parse data as JSON
      })
      .catch(error => {
        console.log(error.error); // error message as string
    });
  }

  logout(){
    this.storage.clear();
    this.router.navigate(['login']);
  }

}
