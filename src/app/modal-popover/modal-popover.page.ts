import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-popover',
  templateUrl: './modal-popover.page.html',
  styleUrls: ['./modal-popover.page.scss'],
})
export class ModalPopoverPage implements OnInit {

  constructor(private modalCtr: ModalController) { }

  ngOnInit() {
  }

  //close modal
  async close() {
    const closeModal: string = "closed";
    await this.modalCtr.dismiss(closeModal);
  }

  //new Post & close modal
  async newPost(form) {
    const post: Array<String> = [form.value.title,form.value.body];
    await this.modalCtr.dismiss(post)
  }

}
