import { Component } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { ModalController } from '@ionic/angular';
import { ModalPopoverPage } from '../modal-popover/modal-popover.page';
import { Storage } from '@ionic/storage-angular';
import { Router } from  "@angular/router";

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  //variables
  posts : string[] = [];
  userDetails;
  userId;
  title;
  body;

  constructor(private router: Router,private http: HTTP,public modalController: ModalController,private storage: Storage) {}

  ngOnInit() {

    //get user data
    this.storage.get('user').then((val) => {
      this.userId = val.id;
      this.userDetails = val;
    });

    //get posts from api
    this.http.get('https://jsonplaceholder.typicode.com/posts', {}, {})
      .then(data => {

        this.posts = JSON.parse(data.data) //parse JSON

      })
      .catch(error => {

        console.log(error.error); // error message

    });
  }

  //Function to open modal
  async presentModal() {
    const modal = await this.modalController.create({
      component: ModalPopoverPage,  
      cssClass: 'my-custom-class'
    });
    modal.onDidDismiss()
      .then((data) => {

        //post data from modal
        const modalData = data['data']; 

        //if the post modal has not just been closed , call post function
        if(modalData!="closed"){
          const post = {
            "userId":this.userId,
            "id":this.posts.length+1,
            "title":modalData[0],
            "body":modalData[1]
          }
          this.newPost(post);
        }
    });
    return await modal.present();
  }

  //logout function
  logout(){
    this.storage.clear();
    this.router.navigate(['login']);
  }

  //new post
  newPost(post : any){
    //store post locally
    this.posts.push(post)
    //post to api
    this.http.post('https://jsonplaceholder.typicode.com/posts', post , {})
      .then(data => {

        console.log("post added"); // success 

      })
      .catch(error => {

        console.log(error.error); // error message

    });
  }

}
