import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage-angular';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  
  users;

  constructor(public toastController: ToastController,private router: Router,private http: HTTP,private storage: Storage){}

  ngOnInit() {
    this.storage.create();
  }

  // on click of login button
  login(form){
    //check if valid email
    if(form.status=="INVALID"){
      this.presentToastInvalidEmail()
    }
    else{
      //check if email exists
      this.http.get('https://jsonplaceholder.typicode.com/users?email='+form.value.email, {}, {})
        .then(data => {
          //full list of users
          this.users = JSON.parse(data.data)

          //check if returned data is empty
          if(this.users.length>0){
            this.storage.set("user", this.users[0]);
            this.router.navigate(['home']);
          }
          //if empty show toast error message
          else{
            this.presentToastLoginError();
          }

        })
        .catch(error => {
          console.log(error.error); // error message as string
      });
    }
  }

  //error message if no user found using ionic toast
  async presentToastLoginError() {
    const toast = await this.toastController.create({
      message: 'Email address not found',
      duration: 2000
    });
    toast.present();
  }

  //error message if no user found using ionic toast
  async presentToastInvalidEmail() {
    const toast = await this.toastController.create({
      message: 'Please enter valid email address',
      duration: 2000
    });
    toast.present();
  }

}
